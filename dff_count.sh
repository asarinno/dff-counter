#!/bin/bash

usage() { printf "Usage:\t$0 [-top <module>] [-a] [-v] 'file_1' 'file_2' ...\n" 1>&2; exit 1; }
help()  { printf "A script for counting the amount of dff in the verilog/system verilog code without synthesis.\n";
			    printf "\n";
				  printf "Usage:\t$0 [arguments] FILES \n";
				  printf "\n";
				  printf "\tFILES                   Load modules from a Verilog file.\n";
				  printf "\t                        The file types can be: '.v', '.sv', '.vhdl'.\n";
				  printf "\t                        If the file have 'include', you don't need to specify them separately.\n";
				  printf "\n";
				  printf "optional arguments:\n";
				  printf "\t--help,    -h           Show this help message and exit\n";
				  printf "\t--top,     -t <module>  Manually specify the name of the top module. \n";
				  printf "\t--verbose, -v           Display more information about the module. \n"
				  printf "\t--all,     -a           Display the information separately for each module \n";
				  printf "\t                        when the option is used, '-v' is set automatically. \n";
				  exit 1;}

# check getopt
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

# option --top/-t requires 1 argument
LONGOPTS=help,top:,verbose,all
OPTIONS=ht:va

! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi

eval set -- "$PARSED"

top='-auto-top' v=n a=n

#echo "ARGS $@"

while true; do
    case "$1" in
        -h|--help)
        		help
            shift
            ;;
        -v|--verbose)
            v=y
            shift
            ;;
        -a|--all)
            a=y
            v=y
            shift
            ;;
        -t|--top)
            top="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

#echo "verbose: $v, all: $a, top: $top"
#echo "files: $@"

# handle non-option arguments
if [[ $# < 1 ]]; then
	usage
fi

read_files=''
for var in "$@"
do
    read_files+='read_verilog -sv '"$var"'; '
done

flatten='flatten'
if [[ "$a" == y ]]; then
	flatten=''
fi

if [[ "$top" != "-auto-top" ]]; then
	top="-top $top"
fi

cmd="$read_files hierarchy $top; proc; techmap "'t:$dff t:$adff;'" opt_clean; $flatten;"

if [[ "$v" == n ]]; then
	yosys -QT -p "$cmd"' select -count t:$_DFF*_' \
	| grep 'objects.' \
	| sed 's/objects./DFFs/'
else
  statistic=n
  IFS=''
  yosys -QT -p "$cmd"' stat;' \
	| while read line 
		do	 
			 if [[ $statistic == y ]]; then
	  			printf "$line\n"
	  			continue
			 fi
			 if [[ $line == *"Printing statistics."* ]]; then
					statistic=y
			 fi
		done
fi

