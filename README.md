# DFF-counter

## Description

This project aims to facilitate the counting of D-Flip-Flops (DFF) in the design process. Based on the [Yosys](https://github.com/YosysHQ/yosys) - open source project.

The report describes the number of DFF for each module and for each clock.

## Requirements

 - Yosys (0.24)

## Usage

``` bash
Usage:  dff_count.sh [arguments] FILES 

  FILES                   Load modules from a Verilog file.
                          The file types can be: '.v', '.sv', '.vhdl'.
                          If the file have 'include', you dont need to specify them separately.

optional arguments:
  --help,    -h           Show this help message and exit
  --top,     -t <module>  Manually specify the name of the top module. 
  --verbose, -v           Display more information about the module.
  --fast,    -f           Faster computation by reducing processing steps.
  --all,     -a           Display the information separately for each module 
                          when the option is used, '-v' is set automatically.

```

## Report example of some projecct

``` bash
=== m01 ===

   Number of wires:                  3
   Number of wire bits:              3
   Number of public wires:           3
   Number of public wire bits:       3
   Number of memories:               0
   Number of memory bits:            0
   Number of processes:              0
   Number of cells:                  1
     $and                            1

=== m03 ===

   Number of wires:                  6
   Number of wire bits:              6
   Number of public wires:           4
   Number of public wire bits:       4
   Number of memories:               0
   Number of memory bits:            0
   Number of processes:              1
   Number of cells:                  2
     $_DFF_P_                        1
     $and                            1

=== top ===

   Number of wires:                 12
   Number of wire bits:             12
   Number of public wires:           9
   Number of public wire bits:       9
   Number of memories:               0
   Number of memory bits:            0
   Number of processes:              1
   Number of cells:                  6
     $_DFF_P_                        2
     $xor                            1
     m01                             1
     m03                             2

=== design hierarchy ===

   top                               1
     m01                             1
     m03                             2

   Number of wires:                 27
   Number of wire bits:             27
   Number of public wires:          20
   Number of public wire bits:      20
   Number of memories:               0
   Number of memory bits:            0
   Number of processes:              3
   Number of cells:                  8
     $_DFF_P_                        4
     $and                            3
     $xor                            1
```

## Comparison

### School RiscV - [github](https://github.com/zhelnio/schoolRISCV)
|                        | **execution time** |       **dff / mem**       |
|:----------------------:|:------------------:|:-------------------------:|
| **Vivado (synthesis)** |       31 sec       | 84 dffs / 37 LUT as Logic |
|  **Verific (netlist)** |       153 ms       |         1108 dffs         |
|     **DFF-counter**    |       115 ms       |  84 dffs / 1024 mem bits  |

### Syntacore SCR-1 - [github](https://github.com/syntacore/scr1)
|                        | **execution time** |             **dff / mem**            |
|:----------------------:|:------------------:|:------------------------------------:|
| **Vivado (synthesis)** |    2 min 43 sec    | 2838 dffs / 5240 LUT as Logic1 latch |
|  **Verific (netlist)** |      26.85 sec     |          527129 dffs 1 latch         |
|     **DFF-counter**    |       2.1 sec      |  1760 dff / 524408 mem bits 1 latch  |

### YRV-plus - [github](https://github.com/yuri-panchul/yrv-plus)
|                        | **execution time** |        **dff / mem**       |
|:----------------------:|:------------------:|:--------------------------:|
| **Vivado (synthesis)** |    11 min 28 sec   |         34487 dffs         |
|  **Verific (netlist)** |      1.97 sec      |         35226 dffs         |
|     **DFF-counter**    |      0.43 sec      | 1435 dffs / 33856 mem bits |

### Open-source GPU (Miaow) - [github](https://github.com/VerticalResearchGroup/miaow)
|                        |       **execution time**       |             **dff / mem**             |
|:----------------------:|:------------------------------:|:-------------------------------------:|
| **Vivado (synthesis)** |          28 min 25 sec         |    32864 dffs / 27322 LUT as Logic    |
|  **Verific (netlist)** |             6.76 sec           |          20970 dffs 142 latch         |
|     **DFF-counter**    | 5 min 12 sec (--fast: 6.2 sec) | 88279 dffs / 110336 mem bits 61 latch |

## Future development

- Add our script to CI to automatically receive a report in the mail.
- Integrate our solution into yosys extensions (c++)
- Count the number of DFF's used in FIFO and Finite automata


